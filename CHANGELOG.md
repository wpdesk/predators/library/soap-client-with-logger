## [1.1.1] - 2025-02-14
### Fixed
- Notice: PHP Deprecated: substr(): Passing null to parameter #1 ($string) of type string is deprecated

## [1.1.0] - 2022-01-14
### Added
- maximum response log entry size

## [1.0.0] - 2020-07-29
### Added
- initial version
