[![pipeline status](https://gitlab.com/wpdesk/soap-client-with-logger/badges/master/pipeline.svg)](https://gitlab.com/wpdesk/soap-client-with-logger/pipelines) 
[![coverage report](https://gitlab.com/wpdesk/soap-client-with-logger/badges/master/coverage.svg?job=unit+test+lastest+coverage)](https://gitlab.com/wpdesk/soap-client-with-logger/commits/master) 
[![Latest Stable Version](https://poser.pugx.org/wpdesk/soap-client-with-logger/v/stable)](https://packagist.org/packages/wpdesk/soap-client-with-logger) 
[![Total Downloads](https://poser.pugx.org/wpdesk/soap-client-with-logger/downloads)](https://packagist.org/packages/wpdesk/soap-client-with-logger) 
[![Latest Unstable Version](https://poser.pugx.org/wpdesk/soap-client-with-logger/v/unstable)](https://packagist.org/packages/wpdesk/soap-client-with-logger) 
[![License](https://poser.pugx.org/wpdesk/soap-client-with-logger/license)](https://packagist.org/packages/wpdesk/soap-client-with-logger) 

#SOAP CLient With Logger

##Installation
composer require --dev wpdesk/soap-client-with-logger

##Usage
```php
    $client = new \WPDesk\SOAP\SoapClientWithLogger(
        new SoapClient(
            $wsdl_url,
            $client_options
        ),
        $logger
    );
```
