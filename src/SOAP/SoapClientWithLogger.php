<?php
/**
 * SOAP Client With Logger
 */

namespace WPDesk\SOAP;

use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

/**
 * Decorator for SOAP Client.
 * Can do SOAP calls and log request and response.
 */
class SoapClientWithLogger {

	/**
	 * @var \SoapClient
	 */
	private $soapClient;

	/**
	 * @var LoggerInterface
	 */
	private $logger;

	/**
	 * @var int
	 */
	private $max_response_log_entry_size;

	/**
	 * @param \SoapClient $soapClient
	 * @param LoggerInterface $logger
	 * @param int $max_response_log_entry_size Maximum response log entry size. -1 for unlimited.
	 */
	public function __construct( \SoapClient $soapClient, LoggerInterface $logger, $max_response_log_entry_size = 2000) {
		$this->soapClient                  = $soapClient;
		$this->logger                      = $logger;
		$this->max_response_log_entry_size = $max_response_log_entry_size;
	}

	/**
	 * @param string $function_name
	 * @param array $arguments
	 *
	 * @return mixed
	 */
	public function __call($function_name, $arguments) {
		try {
			return call_user_func_array([$this->soapClient, $function_name], $arguments);
		} finally {
			$this->logger->info(
				$this->soapClient->__getLastRequestHeaders() . $this->soapClient->__getLastRequest(),
				['type' => 'REQUEST']
			);
			$this->logger->info(
				$this->soapClient->__getLastResponseHeaders() .
				( $this->max_response_log_entry_size === -1 ?
					$this->soapClient->__getLastResponse() :
					substr( $this->soapClient->__getLastResponse() ?? '', 0, $this->max_response_log_entry_size ) . '... truncated at character ' . $this->max_response_log_entry_size
				),
				['type' => 'RESPONSE']
			);
		}
	}

}
